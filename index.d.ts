/// <reference types="node" />
import * as stream from "stream";
import {Framework, Utils} from "floose";
import Schema = Utils.Validation.Schema;
import Server = Framework.Server;

export declare enum HttpMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    PATCH = "PATCH",
    DELETE = "DELETE",
    OPTIONS = "OPTIONS"
}

/**
 * Components
 */

export declare enum RestErrorCode {
    /** The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, too large size, invalid request message framing, or deceptive request routing). */
    BAD_REQUEST = 400,
    /** Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. 401 semantically means "unauthenticated",i.e. the user does not have the necessary credentials. */
    UNAUTHORIZED = 401,
    /** Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micro payment scheme, but that has not happened, and this code is not usually used. Google Developers API uses this status if a particular developer has exceeded the daily limit on requests. */
    PAYMENT_REQUIRED = 402,
    /** The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource. */
    FORBIDDEN = 403,
    /** The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible. */
    NOT_FOUND = 404,
    /** A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource. */
    METHOD_NOT_ALLOWED = 405,
    /** The requested resource is capable of generating only content not acceptable according to the Accept headers sent in the request. */
    NOT_ACCEPTABLE = 406,
    /** The client must first authenticate itself with the proxy. */
    PROXY_AUTHENTICATION_REQUIRED = 407,
    /** The server timed out waiting for the request. */
    REQUEST_TIMEOUT = 408,
    /** Indicates that the request could not be processed because of conflict in the request, such as an edit conflict between multiple simultaneous updates. */
    CONFLICT = 409,
    /** Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a 410 status code, the client should not request the resource in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a "404 Not Found" may be used instead. */
    GONE = 410,
    /** The request did not specify the length of its content, which is required by the requested resource. */
    LENGTH_REQUIRED = 411,
    /** The server does not meet one of the preconditions that the requester put on the request. */
    PRECONDITION_FAILED = 412,
    /** The request is larger than the server is willing or able to process. Previously called "Request Entity Too Large". */
    PAYLOAD_TOO_LARGE = 413,
    /** The URI provided was too long for the server to process. Often the result of too much data being encoded as a query-string of a GET request, in which case it should be converted to a POST request. Called "Request-URI Too Long" previously. */
    URI_TOO_LONG = 414,
    /** The request entity has a media type which the server or resource does not support. For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format. */
    UNSUPPORTED_MEDIA_TYPE = 415,
    /** The client has asked for a portion of the file (byte serving), but the server cannot supply that portion. For example, if the client asked for a part of the file that lies beyond the end of the file. Called "Requested Range Not Satisfiable" previously. */
    RANGE_NOT_SATISFIABLE = 416,
    /** The server cannot meet the requirements of the Expect request-header field. */
    EXPECTATION_FAILED = 417,
    /** This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers. The RFC specifies this code should be returned by teapots requested to brew coffee. This HTTP status is used as an Easter egg in some websites, including Google.com. */
    I_AM_A_TEAPOT = 418,
    /** The request was well-formed but was unable to be followed due to semantic errors. */
    UNPROCESSABLE_ENTITY = 422,
    /** The resource that is being accessed is locked. */
    LOCKED = 423,
    /** The request failed due to failure of a previous request (e.g., a PROPPATCH). */
    FAILED_DEPENDENCY = 424,
    /** The origin server requires the request to be conditional. Intended to prevent "the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict." */
    PRECONDITION_REQUIRED = 428,
    /** The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes. */
    TOO_MANY_REQUESTS = 429,
    /** A server operator has received a legal demand to deny access to a resource or to a set of resources that includes the requested resource. The code 451 was chosen as a reference to the novel Fahrenheit 451. */
    UNAVAILABLE_FOR_LEGAL_REASONS = 451
}
export declare class RestError extends Error {
    constructor(code: RestErrorCode, message?: string);
    readonly code: RestErrorCode;
}

export interface RestRequestModifier {
    type: RestModifierType.REQUEST;
    /** The raw request headers (references request.raw.req.headers). */
    headers?: {
        [key: string]: string;
    };
    /** The request dynamic data stored during processing */
    artifacts?: {
        [key: string]: any;
    };
    /** The request method in upper case (e.g. 'GET', 'POST'). */
    method?: HttpMethod;
    /** An object where each key is a path parameter name with matching value as described in [Path parameters](https://github.com/hapijs/hapi/blob/master/API.md#path-parameters).*/
    params?: {
        [key: string]: string;
    };
    /** The request URI's pathname */
    url?: string;
}
export interface RestResponseModifier {
    type: RestModifierType.RESPONSE;
    /** Sets an HTTP header */
    headers?: {
        [key: string]: string;
    };
    /** Sets the HTTP 'Content-Type' header, should only be used to override the built-in default for each response type. */
    mimeType?: string;
    /** Sets the HTTP 'Content-Length' header (to avoid chunked transfer encoding) */
    bytes?: number;
    /** Sets the 'Content-Type' HTTP header 'charset' property */
    charset?: string;
    /** Overrides the default route cache expiration rule for this response instance, time-to-live value in milliseconds. */
    ttl?: number;
    /** Sets the HTTP code */
    httpCode?: number;
    /**  Sets the HTTP status message (e.g. 'Ok' for status code 200). */
    httpMessage?: string;
    /** The response payload */
    payload?: stream.Readable | Buffer | string | object;
}
export declare enum RestModifierType {
    REQUEST = "request",
    RESPONSE = "response"
}
export declare type RestModifier = RestRequestModifier | RestResponseModifier;

export interface RestRequestInfo {
    /** full request url */
    readonly url: string;
    /** request URI's pathname component. */
    readonly path: string;
    /** request path matches an existing route */
    readonly routeMatch: boolean;
    /** url query string */
    readonly query: {
        [key: string]: string;
    } | string[] | string;
    /** content of the HTTP 'Host' header (e.g. 'example.com:8080'). */
    readonly host: string;
    /** the hostname part of the 'Host' header (e.g. 'example.com'). */
    readonly hostname: string;
    /** request reception timestamp. */
    readonly received: number;
    /** content of the HTTP 'Referrer' (or 'Referer') header. */
    readonly referrer: string;
    /** remote client IP address. */
    readonly remoteAddress: string;
    /** remote client port. */
    readonly remotePort: string;
}
export interface RestRequest {
    /** The raw request headers (references request.raw.req.headers). */
    readonly headers: {
        [key: string]: string;
    };
    /** The request dynamic data stored during processing */
    readonly artifacts: {
        [key: string]: any;
    };
    /** The request method in upper case (e.g. 'GET', 'POST'). */
    readonly method: HttpMethod;
    /** The parsed content-type header. Only available when payload parsing enabled and no payload error occurred. */
    readonly mimeType: string;
    /** An object where each key is a path parameter name with matching value as described in [Path parameters](https://github.com/hapijs/hapi/blob/master/API.md#path-parameters).*/
    readonly params: {
        [key: string]: string;
    };
    /** The request URI's pathname component. */
    readonly info: RestRequestInfo;
    /** The request payload based on the route payload.output and payload.parse settings. */
    readonly payload: stream.Readable | Buffer | string | object;
}

export interface RestResponse {
    /** Sets an HTTP header */
    headers?: {
        [key: string]: string;
    };
    /** Sets the HTTP 'Content-Type' header, should only be used to override the built-in default for each response type. */
    mimeType?: string;
    /** Sets the HTTP 'Content-Length' header (to avoid chunked transfer encoding) */
    bytes?: number;
    /** Sets the 'Content-Type' HTTP header 'charset' property */
    charset?: string;
    /** Overrides the default route cache expiration rule for this response instance, time-to-live value in milliseconds. */
    ttl?: number;
    /** Sets the HTTP code */
    httpCode?: number;
    /**  Sets the HTTP status message (e.g. 'Ok' for status code 200). */
    httpMessage?: string;
    /** The response payload */
    payload: stream.Readable | Buffer | string | object;
}

/**
 * Integrations
 */
export declare interface Route {
    readonly method: HttpMethod;
    readonly path: string;
    readonly isInternal?: boolean;
    readonly description?: string;
    readonly notes?: string;
    /**
     * Validate rest request and throw an exception if invalid
     * @throws ValidationError
     * @param request
     */
    validate?: (request: RestRequest) => Promise<void>;
    execute: (request: RestRequest) => Promise<RestResponse>;
}

export declare enum MiddlewareEvent {
    onRequest = "onRequest",
    onResponse = "onResponse"
}

export declare interface Middleware {
    readonly event: MiddlewareEvent;
    execute: (request: RestRequest, response?: RestResponse) => Promise<RestModifier | void>;
}

/**
 * Server
 */
export declare interface RestServerConfig {
    /**
     * An array of fully qualified domain names that can access to server according to Cross-Origin Resource Sharing.
     * The array can contain any combination of fully qualified origins along with origin strings containing a
     * wildcard '*' character, or a single '*' origin string.
     */
    origins: '*' | string[];
    /**
     * The TCP port the server will listen to
     */
    port: number;
}

export declare class RestServer extends Server {
    readonly configurationValidationSchema: Schema;
    readonly running: boolean;
    init(config: RestServerConfig): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;
    addRoute(route: Route): void;
    addMiddleware(middleware: Middleware): void;
    executeRequest(url: string, method: HttpMethod, payload?: string | Buffer | stream.Readable | {}, headers?: {
        [key: string]: string;
    }, remoteAddress?: string): Promise<RestResponse>;
}